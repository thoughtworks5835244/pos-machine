package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class PosMachine {

    public String printReceipt(String[] barcodes, String[] promotions) {
        var itemLines = toItemLines(barcodes);
        var freeItemLines = getFreeItemLines(itemLines, promotions);
        return formatReceipt(itemLines, freeItemLines);
    }

    private List<ItemLine> toItemLines(String[] barcodes) {
        var itemLines = new ArrayList<ItemLine>();

        for (String barcode : barcodes) {
            var code = barcode.split("-")[0];
            var quantity = 1.0f;
            if (barcode.contains("-")) {
                quantity = Float.parseFloat(barcode.split("-")[1]);
            }
            var itemInfos = PosMachineDataLoader.loadAllItems().get(code).split(",");
            var unitPrice = Float.parseFloat(itemInfos[1]);
            var subTotal = unitPrice * quantity;
            var newItemLine = new ItemLine(code, itemInfos[0], itemInfos[2], unitPrice, quantity, subTotal);
            merge(itemLines, newItemLine);
        }

        return itemLines;
    }

    private void merge(List<ItemLine> itemLines, ItemLine newItemLine) {
        var isFound = new AtomicBoolean(false);

        itemLines.stream()
            .filter(l -> l.getCode().equals(newItemLine.getCode()))
            .findFirst()
            .ifPresent(l -> {
                l.setQuantity(l.getQuantity() + newItemLine.getQuantity());
                l.setSubTotal(l.getSubTotal() + newItemLine.getSubTotal());
                isFound.set(true);
            });

        if (!isFound.get()) {
            itemLines.add(newItemLine);
        }
    }

    private List<FreeItemLine> getFreeItemLines(List<ItemLine> itemLines, String[] promotions) {
        var freeItemLines = new ArrayList<FreeItemLine>();
        var promotionItemCodes = Set.of(promotions);

        itemLines.stream()
            .filter(i -> promotionItemCodes.contains(i.getCode()))
            .filter(i -> i.getQuantity() >= 3)
            .forEach(i -> {
                var freeQuantity = ((int) i.getQuantity()) / 3;
                var freeSubTotal = freeQuantity * i.getUnitPrice();

                freeItemLines.add(new FreeItemLine(
                    i.getName(),
                    i.getUnit(),
                    freeQuantity,
                    freeSubTotal
                ));
            });

        return freeItemLines;
    }

    private float getCartItemValue(List<ItemLine> itemLines) {
        return (float) itemLines.stream()
            .map(ItemLine::getSubTotal)
            .mapToDouble(Float::doubleValue)
            .sum();
    }

    private float getFreeItemValue(List<FreeItemLine> freeItemLines) {
        return (float) freeItemLines.stream()
            .map(FreeItemLine::getValue)
            .mapToDouble(Float::doubleValue)
            .sum();
    }

    private String formatQuantity(float quantity) {
        var isQualityInteger = ((int) (quantity * 10)) % 10 == 0;
        return isQualityInteger
            ? String.valueOf((int) quantity)
            : String.format("%.1f", quantity);
    }

    private String formatReceipt(List<ItemLine> itemLines, List<FreeItemLine> freeItemLines) {
        var receipt = new StringBuilder();

        receipt.append("***<No Profit Store> Shopping List***\n");
        receipt.append("----------------------\n");
        itemLines.forEach(l ->
            receipt.append(
                String.format(
                    "Name: %s, Quantity: %s %s, Unit Price: %.2f(CNY), Subtotal: %.2f(CNY)\n",
                    l.getName(),
                    formatQuantity(l.getQuantity()),
                    l.getQuantity() > 1.0 ? l.getUnit() + "s" : l.getUnit(),
                    l.getUnitPrice(),
                    l.getSubTotal()
                )
            )
        );
        receipt.append("----------------------\n");
        receipt.append("Buy two get one free items:\n");
        freeItemLines.forEach(l ->
            receipt.append(
                String.format(
                    "Name: %s, Quantity: %s %s, Value: %.2f(CNY)\n",
                    l.getName(),
                    formatQuantity(l.getQuantity()),
                    l.getQuantity() > 1.0 ? l.getUnit() + "s" : l.getUnit(),
                    l.getValue()
                )
            )
        );

        receipt.append("----------------------\n");
        var cartTotal = getCartItemValue(itemLines);
        var freeTotal = getFreeItemValue(freeItemLines);
        receipt.append(String.format("Total: %.2f(CNY)\n", cartTotal - freeTotal));
        receipt.append(String.format("Saved: %.2f(CNY)\n", freeTotal));
        receipt.append("**********************\n");

        return receipt.toString();
    }
}
