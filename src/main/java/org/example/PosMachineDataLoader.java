package org.example;

import java.util.Map;

public class PosMachineDataLoader {

    public static Map<String, String> loadAllItems() {
        return Map.of(
            "ITEM000001", "Coca-Cola,3,bottle",
            "ITEM000002", "Sprite,3,bottle",
            "ITEM000003", "Badminton,1,piece",
            "ITEM000004", "Instant noodles,4.5,pack",
            "ITEM000005", "Apple,5.5,pound",
            "ITEM000006", "Banana,4,pound"
        );
    }

    public static String[] loadCart() {
        return new String[]{
            "ITEM000001",
            "ITEM000001",
            "ITEM000001",
            "ITEM000001",
            "ITEM000001",
            "ITEM000003",
            "ITEM000003",
            "ITEM000005-3",
            "ITEM000006-3.4"
        };
    }

    public static String[] loadPromotion() {
        return new String[]{
            "ITEM000001",
            "ITEM000003"
        };
    }
}
