package org.example;

public class FreeItemLine {

    private String name;
    private String unit;
    private float quantity;
    private float value;

    public FreeItemLine(String name, String unit, float quantity, float value) {
        this.name = name;
        this.unit = unit;
        this.quantity = quantity;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
