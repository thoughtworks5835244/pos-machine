package org.example;

public class PosMachineMain {

    public static void main(String[] args) {
        System.out.println(
            new PosMachine().printReceipt(
                PosMachineDataLoader.loadCart(),
                PosMachineDataLoader.loadPromotion()
            )
        );
    }
}