package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PosMachineTest {

    @Test
    public void should_return_receipt_given_5_cola_2_badminton_3_apple() {
        PosMachine machine = new PosMachine();

        String result = "***<No Profit Store> Shopping List***\n" +
            "----------------------\n" +
            "Name: Coca-Cola, Quantity: 5 bottles, Unit Price: 3.00(CNY), Subtotal: 15.00(CNY)\n" +
            "Name: Badminton, Quantity: 2 pieces, Unit Price: 1.00(CNY), Subtotal: 2.00(CNY)\n" +
            "Name: Apple, Quantity: 3 pounds, Unit Price: 5.50(CNY), Subtotal: 16.50(CNY)\n" +
            "Name: Banana, Quantity: 3.4 pounds, Unit Price: 4.00(CNY), Subtotal: 13.60(CNY)\n" +
            "----------------------\n" +
            "Buy two get one free items:\n" +
            "Name: Coca-Cola, Quantity: 1 bottle, Value: 3.00(CNY)\n" +
            "----------------------\n" +
            "Total: 44.10(CNY)\n" +
            "Saved: 3.00(CNY)\n" +
            "**********************\n";

        String[] cart = PosMachineDataLoader.loadCart();
        String[] promotions = PosMachineDataLoader.loadPromotion();
        assertEquals(result, machine.printReceipt(cart, promotions));
    }
}